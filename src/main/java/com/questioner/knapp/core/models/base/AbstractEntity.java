package com.questioner.knapp.core.models.base;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@MappedSuperclass
public class AbstractEntity implements Entity {

    @Setter
    protected Long id;

    @Setter
    @Getter
    @Column
    protected String createdby;

    @Setter
    @Getter
    @Column
    protected String createdat;

    @Setter
    @Getter
    @Column
    protected String updatedby;

    @Setter
    @Getter
    @Column
    protected String updatedat;

    public void populateCreateAttributes() {
        this.createdby = "system";
        java.util.Date dt = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.createdat = sdf.format(dt);
    }

    public void populateUpdateAttributes() {
        this.updatedby = "system";
        java.util.Date dt = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.updatedat = sdf.format(dt);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "AbstractEntity{" +
                "id=" + id +
                ", createdby='" + createdby + '\'' +
                ", createdat='" + createdat + '\'' +
                ", updatedby='" + updatedby + '\'' +
                ", updatedat='" + updatedat + '\'' +
                '}';
    }
}
