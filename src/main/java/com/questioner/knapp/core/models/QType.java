package com.questioner.knapp.core.models;

import com.questioner.knapp.core.models.base.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@SuppressWarnings("unused")
@Entity(name = "knapp_question_type")
public class QType extends AbstractEntity {

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private String comments;

    public QType() {
    }

    public QType(String name, String description, String comments) {
        this.name = name;
        this.description = description;
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "QType{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", comments='" + comments + '\'' +
                "} " + super.toString();
    }
}
