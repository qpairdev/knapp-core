package com.questioner.knapp.core.models;

import com.questioner.knapp.core.models.base.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@SuppressWarnings("unused")
@Entity(name = "knapp_element")
public class QElement extends AbstractEntity {

    @Getter
    @Setter
    private String uiview;

    @Getter
    @Setter
    private String comments;

    private Long qTypeId;

    @Transient
    public Long getqTypeId() {
        return qTypeId;
    }

    public void setqTypeId(Long qTypeId) {
        this.qTypeId = qTypeId;
    }

    private QType qtype;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    @ManyToOne
    @JoinColumn(name = "typeid")
    public QType getQtype() {
        return qtype;
    }

    public void setQtype(QType qtype) {
        this.qtype = qtype;
    }

    @Override
    public String toString() {
        return "QElement{" +
                "uiview='" + uiview + '\'' +
                ", comments='" + comments + '\'' +
                ", qTypeId=" + qTypeId +
                ", qtype=" + qtype +
                "} " + super.toString();
    }
}
