package com.questioner.knapp.core.models.base;

import java.io.Serializable;

public interface Entity extends Serializable {

    Long getId();

    String getCreatedby();

    String getCreatedat();

    String getUpdatedby();

    String getUpdatedat();
}
