package com.questioner.knapp.core.models;

import com.questioner.knapp.core.models.base.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@SuppressWarnings("unused")
@Entity(name = "knapp_question")
public class Question extends AbstractEntity {

    @Getter
    @Setter
    private String text;

    @Getter
    @Setter
    private String description;

    private Long qTypeId;

    @Transient
    public Long getqTypeId() {
        return qTypeId;
    }

    public void setqTypeId(Long qTypeId) {
        this.qTypeId = qTypeId;
    }

    private QType qtype;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    @ManyToOne
    @JoinColumn(name = "typeid")
    public QType getQtype() {
        return qtype;
    }

    public void setQtype(QType qtype) {
        this.qtype = qtype;
    }

    public Question() {
    }

    public Question(String text) {
        this.text = text;
    }

    public Question(String text, String description) {
        this.text = text;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Question{" +
                "text='" + text + '\'' +
                ", description='" + description + '\'' +
                ", qTypeId=" + qTypeId +
                ", qtype=" + qtype +
                "} " + super.toString();
    }
}
